#ifndef I2C_H_
#define I2C_H_

#include <stdint.h>

typedef enum {
	   I2C_OK    =  0,
		 I2C_BUSY  = -1,
		 I2C_ERROR = -2
} i2c_status_t;

i2c_status_t i2c_init(void);
i2c_status_t i2c_send(const uint8_t* src, uint32_t size);
i2c_status_t i2c_recv(uint8_t* dst, uint32_t size);

#endif
