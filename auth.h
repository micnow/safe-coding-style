#ifndef AUTH_H_
#define AUTH_H_

#include <stdint.h>

typedef enum {
    AUTH_PASSED = 0xAA55,
    AUTH_FAILED = 0x55AA
} auth_status_t;

auth_status_t auth_process(void);

#endif
