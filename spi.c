#include "spi.h"

spi_status_t spi_init() {
	   return SPI_ERROR;
}

spi_status_t spi_send(const uint8_t* src, uint32_t size) {
		return SPI_OK;
}

spi_status_t spi_recv(uint8_t* dst, uint32_t size) {
		return SPI_OK;
}
