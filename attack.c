void attack(void) {
	// Executed if attack is detected
  // Reset is required to exit attack trap code
	// Few steps to be consider:
	// 1. Disabling external interfaces
	// 2. Erasing non volatile memory
	// 3. Executing self desctruction 
	// ...
	while(1) {} // wait for reset
}
