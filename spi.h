#ifndef SPI_H_
#define SPI_H_

#include <stdint.h>

typedef enum {
	   SPI_OK    =  0,
		 SPI_BUSY  = -1,
		 SPI_ERROR = -2
} spi_status_t;

spi_status_t spi_init(void);
spi_status_t spi_send(const uint8_t* src, const uint32_t size);
spi_status_t spi_recv(uint8_t* dst, uint32_t size);

#endif
