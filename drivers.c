#include "drivers.h"
#include "i2c.h"
#include "spi.h"

drv_stat_t drv_init() {
	
	if(i2c_init() != I2C_OK) {
		return DRV_ERROR;
	}
	
	if(spi_init() != SPI_OK) {
		return DRV_ERROR;
	}
	
	return DRV_OK;
}
