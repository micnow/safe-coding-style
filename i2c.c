#include "i2c.h"

i2c_status_t i2c_init(void) {
	   return I2C_OK;
}

i2c_status_t i2c_send(const uint8_t* src, uint32_t size) {
	   return I2C_OK;
}

i2c_status_t i2c_recv(uint8_t* dst, uint32_t size) {
	   return I2C_OK;
}
