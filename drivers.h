#include <stdint.h>

typedef enum {
	   DRV_OK    =  0,
		 DRV_BUSY  = -1,
		 DRV_ERROR = -2
} drv_stat_t;

typedef enum {
	  COMM_I2C  = 1,
	  COMM_SPI  = 2
} drv_if_t;

drv_stat_t drv_init(void);

