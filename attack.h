#ifndef ATTACK_H_
#define ATTACK_H_

void attack(void);

#define ATTACK(expr) ((expr>0) ? 0 : attack())

#endif
