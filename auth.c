#include "auth.h"
#include "spi.h"

uint8_t challenge[] = {'G','I','V','E',' ','P','A','S','S'};
uint8_t response[]  = {'B','A','D'};
uint8_t pass[]      = {'P','A','S','S','W','O','R','D'};
	
auth_status_t auth_process(void) {
	uint8_t cntr;
	
	spi_send(challenge, sizeof(challenge));
	spi_recv(response, sizeof(response));
	
	for (cntr=0; cntr<sizeof(response); cntr++) {
		if (pass[cntr] != response[cntr]) {
			return AUTH_FAILED;
		}
	}
	
	return  AUTH_PASSED;
}
